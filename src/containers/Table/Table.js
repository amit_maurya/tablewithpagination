import React, { Component } from 'react';
import ReactTable from 'react-table';
import axios from 'axios';
import JsonData from '../../assets/JsonData/JsonData';
import './Table.css';

class Table extends Component {
    constructor(props) {
        super(props);
        this.state = {
            renderData: JsonData[0].shipments,
            pageSize: 5,
            pageIndex: 0,
            searchInput: '',
        }
    }

    componentDidMount() {

        axios.get(`https://github.com/typicode/json-server/blob/master/db.json`,
            {
                headers: {
                    client_id: '3f146988ff07f9be1864',
                    client_secret: 'a91df0496f13df72a72e9e849954610851b16aa5'

                    // "Access-Control-Allow-Origin": "http://localhost:3000",
                    // Authorization: 'Token a91df0496f13df72a72e9e849954610851b16aa5'
                }
            })

            .then((response) => {
                console.log('response', response)
            })
            .catch((error) => {
                if (error && error.response && error.response.status === 401) {
                    console.log('Error')
                }
            });
    }

    onPageChange = (pageIndex) => {
        this.setState({ pageIndex });
    }

    onPageSizeChange = (pageSize, pageIndex) => {
        this.setState({ pageSize, pageIndex })
    }

    // This function call, when user type text or clear text value in search fields
    inputHandler = (event) => {
        this.setState({ searchInput: event.target.value }, () => {
            if (!this.state.searchInput) {
                this.setState({ renderData: JsonData[0].shipments });
            }
        });
    }

    // OnClick on search button, then this function call and searching data
    searchHandler = () => {

        if (this.state.searchInput) {
            if (this.state.searchInput.length > 0) {
                // eslint-disable-next-line
                const matchData = JsonData[0].shipments.filter(data => {
                    if (data.mode.toLowerCase().includes(this.state.searchInput.toLowerCase())
                        || data.name
                            .toLowerCase()
                            .includes(this.state.searchInput.toLowerCase())
                        || data.origin
                            .toLowerCase()
                            .includes(this.state.searchInput.toLowerCase())
                        || data.total
                            .includes(this.state.searchInput)
                    )
                        return data;
                });
                this.setState({ renderData: matchData })
            }
        }
    }

    // sortByTotalHandler = () => {
    //     console.log('sortByTotalHandler')
    //     this.state.renderData.sort((a, b) => a.total < b.total ? -1 : (a.total > b.total ? 1 : 0))
    // }
    render() {

        return (
            <div>

                <input className="input-text" type="search" placeholder="Search..."
                    value={this.state.searchInput} onChange={(event) => this.inputHandler(event)} />
                <button className="search-btn"
                    type="button" onClick={() => this.searchHandler()}>Search</button>

                <ReactTable
                    data={this.state.renderData ? this.state.renderData : []}
                    columns={[
                        {
                            Header: () => <div className="ID" >ID</div>,
                            accessor: 'id',
                            className: 'text-center',
                            width: 75,
                        },
                        {
                            Header: () => <div className="Header" >Name</div>,
                            accessor: 'name',
                            className: 'text-center',
                            width: 400
                        },
                        {
                            Header: () => <div className="Header" >Mode</div>,
                            accessor: 'mode',
                            className: 'text-center',
                        },
                        {
                            Header: () => <div className="Header" >Origin</div>,
                            accessor: 'origin',
                            className: 'text-center',
                        },
                        {
                            Header: () => <div className="Header" >Total</div>,
                            accessor: 'total',
                            className: 'text-center',
                        },
                    ]}
                    pageSize={this.state.pageSize}
                    showPaginationBottom={true}
                    pageIndex={this.state.pageIndex}
                    onPageChange={(pageIndex) => this.onPageChange(pageIndex)}
                    onPageSizeChange={(pageSize, pageIndex) => { this.onPageSizeChange(pageSize, pageIndex) }}
                />

            </div>
        )
    }
}

export default Table;